package Lab17

import io.gatling.core.Predef.*
import io.gatling.http.Predef.*
import io.gatling.jdbc.Predef.*

import scala.concurrent.duration.*

class GatlingTest extends Simulation {

  private val httpProtocol = http
    .baseUrl("https://gatling.io")
    .inferHtmlResources(AllowList(), DenyList(""".*\.js""", """.*\.css""", """.*\.gif""", """.*\.jpeg""", """.*\.jpg""", """.*\.ico""", """.*\.woff""", """.*\.woff2""", """.*\.(t|o)tf""", """.*\.png""", """.*detectportal\.firefox\.com.*"""))

  private val headers_1 = Map(
    "DNT" -> "1",
    "User-Agent" -> "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100""",
    "sec-ch-ua-mobile" -> "?0",
    "sec-ch-ua-platform" -> "Windows"
  )

  private val headers_7 = Map(
    "accept" -> "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
    "accept-encoding" -> "gzip, deflate, br",
    "accept-language" -> "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
    "dnt" -> "1",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100""",
    "sec-ch-ua-mobile" -> "?0",
    "sec-ch-ua-platform" -> "Windows",
    "sec-fetch-dest" -> "image",
    "sec-fetch-mode" -> "no-cors",
    "sec-fetch-site" -> "same-origin",
    "user-agent" -> "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36"
  )

  private val headers_10 = Map(
    "accept" -> "image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8",
    "accept-encoding" -> "gzip, deflate, br",
    "accept-language" -> "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
    "dnt" -> "1",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100""",
    "sec-ch-ua-mobile" -> "?0",
    "sec-ch-ua-platform" -> "Windows",
    "sec-fetch-dest" -> "image",
    "sec-fetch-mode" -> "no-cors",
    "sec-fetch-site" -> "cross-site",
    "user-agent" -> "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36"
  )

  private val headers_13 = Map(
    "accept" -> "*/*",
    "accept-encoding" -> "gzip, deflate, br",
    "accept-language" -> "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
    "access-control-request-headers" -> "x-hubspot-messages-uri",
    "access-control-request-method" -> "GET",
    "origin" -> "https://gatling.io",
    "sec-fetch-dest" -> "empty",
    "sec-fetch-mode" -> "cors",
    "sec-fetch-site" -> "cross-site",
    "user-agent" -> "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36"
  )

  private val headers_14 = Map(
    "accept" -> "application/json, text/plain, */*",
    "accept-encoding" -> "gzip, deflate, br",
    "accept-language" -> "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
    "dnt" -> "1",
    "origin" -> "https://gatling.io",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100""",
    "sec-ch-ua-mobile" -> "?0",
    "sec-ch-ua-platform" -> "Windows",
    "sec-fetch-dest" -> "empty",
    "sec-fetch-mode" -> "cors",
    "sec-fetch-site" -> "cross-site",
    "user-agent" -> "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36"
  )

  private val headers_15 = Map(
    "accept" -> "*/*",
    "accept-encoding" -> "gzip, deflate, br",
    "accept-language" -> "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
    "dnt" -> "1",
    "origin" -> "https://gatling.io",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100""",
    "sec-ch-ua-mobile" -> "?0",
    "sec-ch-ua-platform" -> "Windows",
    "sec-fetch-dest" -> "empty",
    "sec-fetch-mode" -> "cors",
    "sec-fetch-site" -> "cross-site",
    "user-agent" -> "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36",
    "x-hubspot-messages-uri" -> "https://gatling.io/docs/enterprise/self-hosted/reference/current/plugins/jenkins/"
  )

  private val headers_17 = Map(
    "accept" -> "*/*",
    "accept-encoding" -> "gzip, deflate, br",
    "accept-language" -> "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
    "dnt" -> "1",
    "origin" -> "https://gatling.io",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100""",
    "sec-ch-ua-mobile" -> "?0",
    "sec-ch-ua-platform" -> "Windows",
    "sec-fetch-dest" -> "empty",
    "sec-fetch-mode" -> "cors",
    "sec-fetch-site" -> "cross-site",
    "user-agent" -> "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36"
  )

  private val headers_19 = Map(
    "accept" -> "*/*",
    "accept-encoding" -> "gzip, deflate, br",
    "accept-language" -> "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
    "content-type" -> "text/plain;charset=UTF-8",
    "dnt" -> "1",
    "origin" -> "https://gatling.io",
    "sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100""",
    "sec-ch-ua-mobile" -> "?0",
    "sec-ch-ua-platform" -> "Windows",
    "sec-fetch-dest" -> "empty",
    "sec-fetch-mode" -> "no-cors",
    "sec-fetch-site" -> "cross-site",
    "user-agent" -> "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36"
  )

  private val uri01 = "https://www.google-analytics.com"

  private val uri03 = "https://api.hubapi.com/hs-script-loader-public/v1/config/pixel/json"

  private val uri04 = "https://track.hubspot.com/__ptq.gif"

  private val uri05 = "https://vars.hotjar.com/box-21ccaa45726c0f3c8c458f7a87eb2298.html"

  private val uri06 = "https://www.googletagmanager.com"

  private val uri07 = "https://static.hotjar.com/c/hotjar-2865857.js"

  private val uri08 = "https://forms.hubspot.com/collected-forms/v1/config/json"

  private val uri09 = "https://fonts.googleapis.com/css2"

  private val uri10 = "https://api.hubspot.com/livechat-public/v1/message/public"

  private val uri11 = "https://forms.hsforms.com/embed/v3/counters.gif"

  private val scn = scenario("GatlingTest")
    .exec(
      http("request_0")
        .get("/docs/enterprise/self-hosted/reference/current/plugins/jenkins/")
        .resources(
          http("request_1")
            .get("/docs/images/logo-gatling-noir.svg")
            .headers(headers_1),
          http("request_2")
            .get("/docs/images/logo-gatling.svg")
            .headers(headers_1),
          http("request_3")
            .get(uri06 + "/gtag/js?id=UA-53375088-1"),
          http("request_4")
            .get(uri09 + "?family=DM+Sans:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap"),
          http("request_5")
            .get(uri06 + "/gtm.js?id=GTM-TRJ26ZX"),
          http("request_6")
            .get("/docs/search/index.json"),
          http("request_7")
            .get("/docs/images/alerts/fire-solid.svg")
            .headers(headers_7),
          http("request_8")
            .get(uri06 + "/gtag/js?id=G-B5J9F14X56&l=dataLayer&cx=c"),
          http("request_9")
            .get(uri07 + "?sv=7"),
          http("request_10")
            .get(uri01 + "/collect?v=1&_v=j96&a=445868425&t=pageview&_s=1&dl=https%3A%2F%2Fgatling.io%2Fdocs%2Fenterprise%2Fself-hosted%2Freference%2Fcurrent%2Fplugins%2Fjenkins%2F&ul=ru-ru&de=UTF-8&dt=Gatling%20Enterprise%20Self-Hosted%20-%20Jenkins%20Plugin&sd=24-bit&sr=1536x864&vp=1163x762&je=0&_u=QACAAUAB~&jid=&gjid=&cid=30540488.1649089818&tid=UA-53375088-1&_gid=1148706200.1651691116&gtm=2ou540&z=1755530451")
            .headers(headers_10),
          http("request_11")
            .get(uri01 + "/collect?v=1&_v=j96&a=445868425&t=event&_s=2&dl=https%3A%2F%2Fgatling.io%2Fdocs%2Fenterprise%2Fself-hosted%2Freference%2Fcurrent%2Fplugins%2Fjenkins%2F&ul=ru-ru&de=UTF-8&dt=Gatling%20Enterprise%20Self-Hosted%20-%20Jenkins%20Plugin&sd=24-bit&sr=1536x864&vp=1163x762&je=0&ec=Documentation%20DSL&ea=Language%20defined%20on%20pageview&el=Java&_u=SACAAUAB~&jid=&gjid=&cid=30540488.1649089818&tid=UA-53375088-1&_gid=1148706200.1651691116&gtm=2ou540&z=1009136909")
            .headers(headers_10),
          http("request_12")
            .get(uri05),
          http("request_13")
            .options(uri10 + "?portalId=8006059&conversations-embed=static-1.10012&mobile=false&messagesUtk=7e6d060f53f0438bad83c2fcbbfdbd96&traceId=7e6d060f53f0438bad83c2fcbbfdbd96&hubspotUtk=e0283a8baf41204d69e7e8cbf26422b4&__hstc=73906564.e0283a8baf41204d69e7e8cbf26422b4.1649151579350.1651691116769.1651730863276.4&__hssc=73906564.10.1651730863276&referrer=https%3A%2F%2Fgatling.io%2Fdocs%2Fenterprise%2Fself-hosted%2F")
            .headers(headers_13),
          http("request_14")
            .get(uri08 + "?portalId=8006059&utk=e0283a8baf41204d69e7e8cbf26422b4")
            .headers(headers_14),
          http("request_15")
            .get(uri10 + "?portalId=8006059&conversations-embed=static-1.10012&mobile=false&messagesUtk=7e6d060f53f0438bad83c2fcbbfdbd96&traceId=7e6d060f53f0438bad83c2fcbbfdbd96&hubspotUtk=e0283a8baf41204d69e7e8cbf26422b4&__hstc=73906564.e0283a8baf41204d69e7e8cbf26422b4.1649151579350.1651691116769.1651730863276.4&__hssc=73906564.10.1651730863276&referrer=https%3A%2F%2Fgatling.io%2Fdocs%2Fenterprise%2Fself-hosted%2F")
            .headers(headers_15),
          http("request_16")
            .get(uri04 + "?k=1&sd=1536x864&cd=24-bit&cs=UTF-8&ln=ru-ru&bfp=1437420187&v=1.1&a=8006059&rcu=https%3A%2F%2Fgatling.io%2Fdocs%2Fenterprise%2Fself-hosted%2Freference%2Fcurrent%2Fplugins%2Fjenkins%2F&r=https%3A%2F%2Fgatling.io%2Fdocs%2Fenterprise%2Fself-hosted%2F&pu=https%3A%2F%2Fgatling.io%2Fdocs%2Fenterprise%2Fself-hosted%2Freference%2Fcurrent%2Fplugins%2Fjenkins%2F&t=Gatling+Enterprise+Self-Hosted+-+Jenkins+Plugin&cts=1651731080517&vi=e0283a8baf41204d69e7e8cbf26422b4&nc=false&u=73906564.e0283a8baf41204d69e7e8cbf26422b4.1649151579350.1651691116769.1651730863276.4&b=73906564.11.1651730863276&pt=1&cc=15")
            .headers(headers_10),
          http("request_17")
            .get(uri03 + "?portalId=8006059")
            .headers(headers_17),
          http("request_18")
            .get(uri11 + "?key=collected-forms-embed-js-form-bind&count=1")
            .headers(headers_10)
        )
    )
    .pause(4)
    .exec(
      http("request_19")
        .post(uri01 + "/g/collect?v=2&tid=G-B5J9F14X56&gtm=2oe540&_p=445868425&_z=ccd.tbB&cid=30540488.1649089818&ul=ru-ru&sr=1536x864&sid=1651730859&sct=7&seg=1&dl=https%3A%2F%2Fgatling.io%2Fdocs%2Fenterprise%2Fself-hosted%2Freference%2Fcurrent%2Fplugins%2Fjenkins%2F&dr=https%3A%2F%2Fgatling.io%2Fdocs%2Fenterprise%2Fself-hosted%2F&dt=Gatling%20Enterprise%20Self-Hosted%20-%20Jenkins%20Plugin&_s=1")
        .headers(headers_19)
        .body(RawFileBody("gatlingtest/0019_request.txt"))
    )

  setUp(
    scn.inject(
    nothingFor(4),
    atOnceUsers(2),
    rampUsers(3).during(5),
      constantUsersPerSec(1).during(5)
  ).protocols(httpProtocol))
}
